<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
use App\Model\Item;

class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        for ($i = 0; $i < 30; $i++) {
            Item::create([
                'name' => $faker->sentence(4),
                'description' => $faker->text,
            ]);
        }
    }
}
