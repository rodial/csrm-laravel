<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;

/**
 * Item crud test.
 */
class ItemTest extends TestCase
{
    use WithFaker;

    private static $_name;
    private static $_desc;
    private static $_id;

    public function setUp()
    {
        parent::setUp();

        if (empty(self::$_name)) {
            $this->setUpFaker();

            self::$_name = 'Test ' . $this->faker->sentence(3);
            self::$_desc = 'Test. ' . $this->faker->text;
        }

    }

    public function testCreate()
    {
        $responseArr = $this->jsonArr('POST', '/api/v1/item', 201, [
            'name' => self::$_name,
            'description' => self::$_desc,
        ]);

        $this->assertEquals($responseArr['name'], self::$_name);
        $this->assertEquals($responseArr['description'], self::$_desc);

        self::$_id =  $responseArr['id'];
    }

    /**
     * @depends testCreate
     */
    public function testShow()
    {
        $responseArr = $this->jsonArr('GET', '/api/v1/item/' . self::$_id);

        $this->assertEquals($responseArr['name'], self::$_name);
        $this->assertEquals($responseArr['description'], self::$_desc);
    }

    /**
     * @depends testShow
     */
    public function testUpdate()
    {
        $responseArr = $this->jsonArr('PUT', '/api/v1/item/' . self::$_id, 200, [
            'name' => self::$_name . 'test',
        ]);

        $this->assertEquals($responseArr['name'], self::$_name . 'test');
    }

    /**
     * @depends testUpdate
     */
    public function testIndex()
    {
        $responseArr = $this->jsonArr('GET', '/api/v1/item');

        $this->assertNotEmpty($responseArr['items']);
    }

    /**
     * @depends testIndex
     */
    public function testDeleteAnd404()
    {
        $this->jsonArr('DELETE', '/api/v1/item/' . self::$_id);
        $responseArr = $this->jsonArr('DELETE', '/api/v1/item/' . self::$_id, 404);

        $this->assertNotEmpty($responseArr['error']);
    }

    private function jsonArr($method, $uri, $status = 200, array $data = [], array $headers = [])
    {
        $response = parent::json($method, $uri, $data, $headers);

        $response->assertStatus($status);
        $response->assertJsonStructure();

        $this->assertNotEmpty($response->getContent());

        return json_decode($response->getContent(), true);
    }
}
