#!/usr/bin/env bash

if [ ! -f /var/www/.env ]; then
    cp /var/www/.env.example /var/www/.env
    php artisan key:generate
fi

if [ ! -f /var/www/.env ]; then
    echo "File .env was not found!"
    exit 1
fi

php artisan migrate || {
    echo "php artisan migrate command failed!"
    exit 1
}

php-fpm
