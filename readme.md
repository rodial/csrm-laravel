# Laravel контроллер -> сервис -> репозиторий -> модель

Для разворачивания проекта выполнить в консоли в корневой папке задания следующие команды:

```bash
docker-compose up -d
```

Для создания записей

```bash
docker-compose exec fpm php artisan db:seed
```

Запуск тестов

```bash
docker-compose exec fpm vendor/bin/phpunit
```

При запущеном контейнере можно использовать

```bash
docker-compose run composer install
```

```bash
docker-compose exec fpm php artisan migrate
```

go to [http://localhost:8089/api/v1/item](http://localhost:8089/api/v1/item)

