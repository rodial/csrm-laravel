<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Model\Item;
use App\Repository\Eloquent\ItemRepository;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    protected $rep;

    public function __construct(ItemRepository $rep)
    {
        $this->rep = $rep;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $perPage = $request->get('per_page');
        $page = $request->get('page');

        $items = $this->rep->paginate($page, $perPage);

        return [
            'items' => $items->items(),
            'page' => $items->currentPage(),
            'pages' => $items->lastPage(),
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $item = $this->rep->create($request->all());

        return $item;
    }

    /**
     * Display the specified resource.
     *
     * @param  Item $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        return $this->rep->find($item->id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Item $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $item)
    {
        $this->rep->update($request->all(), $item->id);

        return $this->rep->find($item->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Item $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        $this->rep->delete($item->id);
        //$item->delete();

        return [
            'status' => 'ok',
        ];
    }
}
