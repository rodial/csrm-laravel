<?php

namespace App\Repository\Eloquent;

use App\Model\Item;

class ItemRepository extends BaseRepository
{
    function model()
    {
        return Item::class;
    }
}