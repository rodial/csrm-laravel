<?php

namespace App\Repository\Eloquent;

use App\Repository\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Container\Container;

/**
 * Class BaseRepository
 *
 * @package App\Repository\Eloquent
 */
abstract class BaseRepository implements RepositoryInterface
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @var Model
     */
    protected $model;

    /**
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->model = $this->makeModel();
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    abstract function model();

    /**
     * @inheritdoc
     */
    public function all(array $columns = ['*'])
    {
        return $this->model->get($columns);
    }

    /**
     * {@inheritdoc}
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginate(int $page = null, ?int $perPage = 10, array $columns = ['*'], $pageName = 'page')
    {
        return $this->model->paginate($perPage, $columns, $pageName, $page);
    }

    /**
     * {@inheritdoc}
     *
     * @return Model
     */
    public function create(array $data)
    {
        return $this->model->create($data);
    }

    /**
     * {@inheritdoc}
     *
     * @return int
     */
    public function update(array $data, int $id, $attribute = "id")
    {
        return $this->model->where($attribute, '=', $id)->update($data);
    }

    /**
     * {@inheritdoc}
     *
     * @return int
     */
    public function delete(int $id)
    {
        //return $this->model->destroy($id);
        return $this->model()::destroy($id);
    }

    /**
     * {@inheritdoc}
     *
     * @return \Illuminate\Database\Eloquent\Collection|Model|null|static|static[]
     */
    public function find(int $id, array $columns = ['*'])
    {
        return $this->model->find($id, $columns);
    }

    /**
     * {@inheritdoc}
     *
     * @return Model|null|object|static
     */
    public function findBy($attribute, $value, array $columns = ['*'])
    {
        return $this->model->where($attribute, '=', $value)->first($columns);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     * @throws \Exception
     */
    public function makeModel()
    {
        $model = $this->container->make($this->model());

        if (!$model instanceof Model)
            throw new \Exception("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");

        return $this->model = $model->newQuery();
    }
}