<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    public $timestamps = false;

    protected $fillable = ['name', 'description'];

    protected $visible = ['id', 'name', 'description'];
}
